
#include <gtest/gtest.h>

#include "SceneGraph.h"

using glm::vec3;

std::ostream& operator<<(std::ostream& os, const vec3& v)
{
	return os << v.x << ", " << v.y << ", " << v.z;
}

//-------------------------------------------------

TEST(SceneGraph, Construction)
{
	SceneGraph sg;
	ASSERT_EQ(0, sg.GetNodeCount());
}

// Requirement 3.a
TEST(SceneGraph, PushBackNode)
{
	SceneGraph sg;
	const Transform transform {glm::vec3(1, 0, 0)};
	auto nh = sg.PushBackNode("node", transform);
	ASSERT_EQ(1, sg.GetNodeCount());
	ASSERT_EQ("node", sg.GetNodeID(nh));
}

// Requirements 3.a, 3.c
TEST(SceneGraph, AddNodeWithParent)
{
	SceneGraph sg;
	auto parent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto child = sg.PushBackNode("child", Transform(vec3(0, 1, 0)), parent);
	
	ASSERT_EQ(2, sg.GetNodeCount());
	ASSERT_EQ("parent", sg.GetNodeID(parent));
	ASSERT_EQ("child", sg.GetNodeID(child));
}

// Requirement 3.f
TEST(SceneGraph, GetWorldTransform)
{
	SceneGraph sg;
	const Transform transform {glm::vec3(1, 0, 0)};
	auto nh = sg.PushBackNode("node", transform);
	ASSERT_EQ(transform.position, sg.GetWorldPosition(nh));
}

// Requirement 3.b, 3.e
TEST(SceneGraph, UpdateWorldTransforms)
{
	SceneGraph sg;
	auto nh1 = sg.PushBackNode("node1", Transform(vec3(1, 0, 0)));
	auto nh2 = sg.PushBackNode("node2", Transform(vec3(0, 0, 0)));
	sg.SetLocalPosition(nh1, vec3(0, 1, 0));
	sg.SetLocalPosition(nh2, vec3(1, 2, 3));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(vec3(0, 1, 0), sg.GetWorldPosition(nh1));
	ASSERT_EQ(vec3(1, 2, 3), sg.GetWorldPosition(nh2));
}

TEST(SceneGraph, FindNodeExisting)
{
	SceneGraph sg;
	auto nh = sg.PushBackNode("findme", Transform());

	ASSERT_EQ(nh, sg.FindNode("findme"));
}

TEST(SceneGraph, FindNodeNotExisting)
{
	SceneGraph sg;
	auto nh = sg.PushBackNode("cantfindme", Transform());

	ASSERT_EQ(InvalidNodeHandle, sg.FindNode("areyouthere"));
}

// Requirement 3.b
TEST(SceneGraph, SetLocalTransform)
{
	SceneGraph sg;
	auto nh = sg.PushBackNode("node", Transform(vec3(1, 0, 0)));
	sg.SetLocalPosition(nh, vec3(0, 1, 0));

	ASSERT_EQ(vec3(0, 1, 0), sg.GetLocalPosition(nh));
}

// Requirements 3.b, 3.c
TEST(SceneGraph, SetLocalTransformChild)
{
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto hchild = sg.PushBackNode("child", Transform(vec3(0, 1, 0)), hparent);
	sg.SetLocalPosition(hchild, glm::vec3(0, 0, 10));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(glm::vec3(1, 0, 10), sg.GetWorldPosition(hchild));
}

// Requirements 3.b, 3.c
TEST(SceneGraph, SetLocalTransformChild2)
{
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto hchild = sg.PushBackNode("child", Transform(vec3(0, 1, 0)), hparent);
	sg.SetLocalPosition(hchild, glm::vec3(1, 2, 3));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(glm::vec3(2, 2, 3), sg.GetWorldPosition(hchild));
}

// Requirements 3.b, 3.c
TEST(SceneGraph, SetLocalTransformParent)
{
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto hchild = sg.PushBackNode("node", Transform(vec3(0, 1, 0)), hparent);
	sg.SetLocalPosition(hparent, vec3(1, 2, 3));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(vec3(1, 2, 3), sg.GetWorldPosition(hparent));
	ASSERT_EQ(vec3(1, 3, 3), sg.GetWorldPosition(hchild));
}

// Requirements 3.b, 3.c
TEST(SceneGraph, SetLocalTransformWithParentRotation)
{
	// Parent at origin rotated 90 deg in Y, child with local position 0,0,1
	// should have world position 1,0,0
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform());
	auto hchild = sg.PushBackNode("node", Transform(vec3(0, 0, 1)), hparent);
	glm::quat rot(glm::vec3(0, glm::half_pi<float>(), 0));
	sg.SetLocalTransform(hparent, Transform(glm::vec3(), rot));
	sg.UpdateWorldTransforms();
	
	ASSERT_EQ(vec3(0, 0, 0), sg.GetWorldPosition(hparent));
	glm::vec3 childPos = sg.GetWorldPosition(hchild);
	ASSERT_NEAR(1, childPos.x, glm::epsilon<float>());
	ASSERT_NEAR(0, childPos.y, glm::epsilon<float>());
	ASSERT_NEAR(0, childPos.z, glm::epsilon<float>());
	ASSERT_EQ(sg.GetWorldRotation(hparent), sg.GetWorldRotation(hchild));
}

TEST(SceneGraph, SetParent)
{
	SceneGraph sg;
	auto hchild = sg.PushBackNode("node", Transform(vec3(0, 1, 0)));
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	sg.SetParent(hchild, hparent);
	// Parenting invalidates handles, need to find the nodes again
	hchild = sg.FindNode("node");
	hparent = sg.FindNode("parent");
	sg.SetLocalPosition(hparent, vec3(1, 2, 3));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(vec3(1, 2, 3), sg.GetWorldPosition(hparent));
	ASSERT_EQ(vec3(1, 3, 3), sg.GetWorldPosition(hchild));
}

TEST(SceneGraph, UnsetParent)
{
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto hchild = sg.PushBackNode("node", Transform(vec3(0, 1, 0)), hparent);
	sg.SetParent(hchild, InvalidNodeHandle);
	// Parenting invalidates handles, need to find the nodes again
	hparent = sg.FindNode("parent");
	hchild = sg.FindNode("node");
	sg.SetLocalPosition(hparent, vec3(1, 2, 3));
	sg.UpdateWorldTransforms();

	ASSERT_EQ(vec3(1, 2, 3), sg.GetWorldPosition(hparent));
	ASSERT_EQ(vec3(0, 1, 0), sg.GetWorldPosition(hchild));
}

// Requirement 3.d
TEST(SceneGraph, RemoveNode)
{
	SceneGraph sg;
	auto hnode = sg.PushBackNode("node", Transform(vec3(1, 0, 0)));
	sg.RemoveNode(hnode);
	sg.UpdateWorldTransforms();
	
	ASSERT_EQ(0, sg.GetNodeCount());
}

// Requirement 3.d
TEST(SceneGraph, RemoveNodeWithChildren)
{
	SceneGraph sg;
	auto hparent = sg.PushBackNode("parent", Transform(vec3(1, 0, 0)));
	auto hchild = sg.PushBackNode("child", Transform(vec3(0, 1, 0)), hparent);
	sg.RemoveNode(hparent);
	sg.UpdateWorldTransforms();
	
	// Removing invalidates handles, need to find the child again
	hchild = sg.FindNode("child");

	ASSERT_EQ(1, sg.GetNodeCount());
	ASSERT_EQ(InvalidNodeHandle, sg.GetNodeParent(hchild));
}

// Requirement 3.d
TEST(SceneGraph, RemoveNodeAndPreserveReferences)
{
	SceneGraph sg;
	auto hparent1 = sg.PushBackNode("parent1", Transform());
	auto hparent2 = sg.PushBackNode("parent2", Transform());
	auto hchild2 = sg.PushBackNode("child2", Transform(), hparent2);
	auto hgrandchild = sg.PushBackNode("grandchild", Transform(), hchild2);
	auto hchild1 = sg.PushBackNode("child1", Transform(), hparent1);
	sg.RemoveNode(hparent2);
	sg.UpdateWorldTransforms();
	
	// Removing invalidates handles, need to find the nodes again
	hparent1 = sg.FindNode("parent1");
	hchild1 = sg.FindNode("child1");
	hchild2 = sg.FindNode("child2");
	hgrandchild = sg.FindNode("grandchild");
	
	ASSERT_EQ(hchild2, sg.GetNodeParent(hgrandchild));
	ASSERT_EQ(hparent1, sg.GetNodeParent(hchild1));
}

TEST(SceneGraph, Optimize)
{
	SceneGraph sg;
	auto hparent1 = sg.PushBackNode("parent1", Transform());
	auto hparent2 = sg.PushBackNode("parent2", Transform());
	auto hfiller = sg.PushBackNode("filler", Transform());
	auto hchild2 = sg.PushBackNode("child2", Transform(), hparent2);
	auto hchild1 = sg.PushBackNode("child1", Transform(), hparent1);
	sg.Optimize();
	
	hparent1 = sg.FindNode("parent1");
	hparent2 = sg.FindNode("parent2");
	hfiller = sg.FindNode("filler");
	hchild1 = sg.FindNode("child1");
	hchild2 = sg.FindNode("child2");

	ASSERT_EQ(InvalidNodeHandle, sg.GetNodeParent(hparent1));
	ASSERT_EQ(InvalidNodeHandle, sg.GetNodeParent(hparent2));
	ASSERT_EQ(InvalidNodeHandle, sg.GetNodeParent(hfiller));
	ASSERT_EQ(hparent1, sg.GetNodeParent(hchild1));
	ASSERT_EQ(hparent2, sg.GetNodeParent(hchild2));
}

//-------------------------------------------------

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
