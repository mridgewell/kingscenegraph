
#include "SceneGraph.h"

#include <algorithm>
#include <chrono>
#include <iostream>

static const int numNodes = 1000000;
static const int runCount = 100;

float RandomFloat(float min, float max)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff   = max - min;
	float r      = random * diff;
	return min + r;
}

int RandomInt(int min, int max)
{
	if (min == max) return min;
	return rand() % (max - min) + min;
}

bool RandomBool(float chance) { return RandomFloat(0.0f, 1.0f) < chance; }

class Profiler
{
public:
	Profiler(std::string name, int runs = 1) :
		profileName{std::move(name)},
		runCount{runs}
	{
		start = std::chrono::high_resolution_clock::now();
	}

	~Profiler()
	{
		const auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<float, std::milli> duration = end - start;
		auto averageDuration = duration.count() / runCount;
		std::cout << profileName << ": " << averageDuration << " ms\n";
	}

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
	std::string profileName;
	int runCount;
};

static void AddNode(SceneGraph& sg, int depth = 0)
{
	static const int depthLimit = 5;

	if (sg.GetNodeCount() == numNodes)
		return;

	Transform transform;
	transform.position.x = RandomFloat(-5.0f, 5.0f);
	transform.position.y = RandomFloat(-5.0f, 5.0f);
	transform.position.z = RandomFloat(-5.0f, 5.0f);

	NodeHandle parent = InvalidNodeHandle;
	if (depth < depthLimit && RandomBool(0.5f))
	{
		int numChildren = RandomInt(1, 3);

		for (int i = 0; i < numChildren; i++)
		{
			AddNode(sg, depth + 1);
			if (sg.GetNodeCount() == numNodes)
				return;
		}
	}

	sg.InsertNode("", transform, parent);
}

static void BuildScene(SceneGraph& sg)
{
	sg.Reserve(numNodes);

	for (int i = 0; i < numNodes; i++)
	{
		AddNode(sg);
	}

	std::cout << "Built scene\n";
}

static void TestUpdateWorldTransforms(SceneGraph& sg)
{
	Profiler prof("UpdateWorldTransforms", runCount);
	for (int i = 0; i < runCount; i++)
	{
		sg.UpdateWorldTransforms();
	}
}

static void TestRender(const SceneGraph& sg)
{
	Profiler prof("Render", runCount);
	volatile int numRenderered = 0;
	for (int i = 0; i < runCount; i++)
	{
		sg.IterateWorldTransforms([&](const Transform&) {
			numRenderered++;
		});
	}
	std::cout << numRenderered << " nodes rendered\n";
}

int main()
{
	SceneGraph sg;
	BuildScene(sg);

	TestUpdateWorldTransforms(sg);
	TestRender(sg);

	return 0;
}
