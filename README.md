King Scene Graph implementation
===============================
Implementation of a scene graph represented by nodes with 3D transforms. The graph implementation can be found in KingSceneGraph/SceneGraph.h. Build in Visual Studio and run Bin/GLDemo.exe to see 3D demo.

![Screenshot of GLDemo](Screenshot.png)

The nodes are stored in a SoA format, using contiguous blocks of memory. Nodes are represented by ID (in this case, a string), local transform, world transform, and handle to a parent. The scene graph attempts to store its nodes in an order determined by the parent-child relationship. Parents are always stored before children so as to only require a single pass to update world transforms. In addition, children are stored as close as possible to their parents, to maximise the chance of parent transforms being in the cache when they are needed.

Requirements
------------
Visual Studio 2017

[Windows 10 SDK version 10.0.17763.0](https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk)

All 3rd party libraries (listed below) are provided as Nuget packages, so should work out-of-the-box.
- GLM OpenGL Mathematics library
- OpenGL/GLUT (for 3D demo)
- Google Test (for unit tests)

Project structure
-----------------
- **KingSceneGraph/** - Library containing the scene graph implementation
- **Test/** - Unit tests for the scene graph library
- **PerfTest/** - Performance test for batch updating 1,000,000 nodes
- **GLDemo/** - Visualisation of the scene graph using cubes in an OpenGL scene

Requirements
------------
##### 1. A constant number of memory allocations. O(1) and not O(n) where n is number of nodes
This can be acheived using the Reserve() function of the SceneGraph with the desired size. Four allocations will be made, and no more are necessary as long as the number of nodes does not exceed this value.

##### 2. A batch oriented update of the matrices
This is acheived using the UpdateLocalTransforms() function template. Any user-defined logic can be applied to all of the node's local transforms.

##### 3. Operations on the scene graph
##### a. Add node
SceneGraph::InsertNode() for faster iteration and SceneGraph::PushBackNode() for faster insertion.

##### b. Set local position and orientation for a node
SceneGraph::SetLocalTransform(), SetLocalPosition(), SetLocalRotation().

##### c. Set parent/child relationship
SceneGraph::SetParent() - use InvalidNodeHandle to remove parent relationship.

##### d. Remove node. Child nodes should not be removed
SceneGraph::RemoveNode().

##### e. Update all world transforms
SceneGraph::UpdateWorldTransforms().

##### f. Get world transform for node
This can be done for a specific node using GetWorldPosition()/GetWorldRotation(), or for all nodes using the function template IterateWorldTransforms().

##### 4. A visualisation of the scene graph structure with for example OpenGL with glut or glfw
See GLDemo.