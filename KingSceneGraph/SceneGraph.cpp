
#include <algorithm>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>

#include "SceneGraph.h"

// Add a node to the end of the list
NodeHandle SceneGraph::PushBackNode(NodeID id, const Transform& localTransform, NodeHandle parenth)
{
	nodeCount++;
	ids.push_back(id);
	localTransforms.push_back(localTransform);
	worldTransforms.push_back(localTransform);
	parents.push_back(parenth);
	return nodeCount - 1;
}

// Add a node to the correct position in the list, preserving the parent-child order
// Invalidates existing handles!
NodeHandle SceneGraph::InsertNode(NodeID id, const Transform& localTransform, NodeHandle parenth)
{
	if (parenth == InvalidNodeHandle)
	{
		return PushBackNode(id, localTransform, parenth);
	}
	else
	{
		nodeCount++;
		auto insertPos = parenth + 1;
		ids.insert(ids.begin() + insertPos, id);
		localTransforms.insert(localTransforms.begin() + insertPos, localTransform);
		worldTransforms.insert(worldTransforms.begin() + insertPos, localTransform);
		parents.insert(parents.begin() + insertPos, parenth);
		return insertPos;
	}
}

// Remove a node from the list
// Invalidates existing handles!
void SceneGraph::RemoveNode(NodeHandle nodeh)
{
	const NodeHandle newParent = parents[nodeh];
	ids.erase(ids.begin() + nodeh);
	localTransforms.erase(localTransforms.begin() + nodeh);
	worldTransforms.erase(worldTransforms.begin() + nodeh);
	parents.erase(parents.begin() + nodeh);
	nodeCount--;

	// Find children and update parent handle
	for (NodeHandle i = nodeh; i < nodeCount; i++)
	{
		if (parents[i] == nodeh)
		{
			parents[i] = newParent;
		}
		else if (parents[i] > nodeh)
		{
			parents[i]--;
		}
	}
}

NodeHandle SceneGraph::FindNode(const NodeID& id)
{
	auto it = std::find_if(ids.begin(), ids.end(), [&id](const NodeID& a) { return a == id; });
	if (it != ids.end())
		return distance(ids.begin(), it);
	return InvalidNodeHandle;
}

void SceneGraph::Reserve(std::size_t size)
{
	ids.reserve(size);
	localTransforms.reserve(size);
	worldTransforms.reserve(size);
	parents.reserve(size);
}

// Sort the nodes into the correct parent-child order. Descendents should follow immediately after their parents.
// Invalidates existing handles!
void SceneGraph::Optimize()
{
	for (std::size_t i = 0; i < nodeCount; i++)
	{
		if (parents[i] != InvalidNodeHandle && parents[i] != i - 1)
		{
			MoveNode(i, parents[i] + 1);
		}
	}
}

void SceneGraph::SetLocalTransform(NodeHandle nodeh, const Transform& transform)
{
	localTransforms[nodeh] = transform;
}

void SceneGraph::SetLocalPosition(NodeHandle nodeh, const glm::vec3& position)
{
	localTransforms[nodeh].position = position;
}

void SceneGraph::SetLocalRotation(NodeHandle nodeh, const glm::quat& rotation)
{
	localTransforms[nodeh].rotation = rotation;
}

// Reparent a node under another node, or remove parent if parenth == InvalidNodeHandle
// Invalidates existing handles!
void SceneGraph::SetParent(NodeHandle nodeh, NodeHandle parenth)
{
	parents[nodeh] = parenth;
	if (parenth != InvalidNodeHandle)
	{
		// Parents cannot appear before children, so move to just after the parent
		MoveNode(nodeh, parenth + 1);
	}
}

glm::vec3 SceneGraph::GetLocalPosition(NodeHandle nodeh)
{
	return localTransforms[nodeh].position;
}

glm::vec3 SceneGraph::GetWorldPosition(NodeHandle nodeh)
{
	return worldTransforms[nodeh].position;
}

glm::quat SceneGraph::GetWorldRotation(NodeHandle nodeh)
{
	return worldTransforms[nodeh].rotation;
}

// Batch calculate all world transforms based on local and parent transforms
void SceneGraph::UpdateWorldTransforms()
{
	for (std::size_t i = 0; i < nodeCount; i++)
	{
		Transform& worldTransform = worldTransforms[i];
		if (parents[i] == InvalidNodeHandle)
		{
			worldTransforms[i].position = localTransforms[i].position;
			worldTransforms[i].rotation = localTransforms[i].rotation;
		}
		else
		{
			const auto parent = parents[i];
			worldTransforms[i].rotation = worldTransforms[parent].rotation * localTransforms[i].rotation;
			worldTransforms[i].position = worldTransforms[parent].position + worldTransforms[parent].rotation * localTransforms[i].position;
		}
	}
}

//-------------------------------------------------

// Move a node to a different position, and update all references to that node
void SceneGraph::MoveNode(NodeHandle node, NodeHandle pos)
{
	if (node == pos)
		return;

	NodeHandle first, mid, last;
	std::ptrdiff_t direction;
	if (pos < node)
	{
		first = pos; mid = node; last = node + 1;
		direction = 1;
	}
	else
	{
		first = node; mid = node + 1; last = pos;
		direction = -1;
	}

	// Move child to just after parent
	auto rotate = [&](auto& nodes) {
		std::rotate(nodes.begin() + first, nodes.begin() + mid, nodes.begin() + last);
	};

	rotate(ids);
	rotate(localTransforms);
	rotate(worldTransforms);
	rotate(parents);
	
	// Update all references to nodes in moved region
	for (std::size_t i = first; i < nodeCount; i++)
	{
		NodeHandle& parent = parents[i];
		if (parent == node)
			parent = pos;
		else if (parent >= first && parent < last)
			parent += direction;
	}
}
