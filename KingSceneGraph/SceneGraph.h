
#pragma once

#include <string>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

using NodeID = std::string;
using NodeHandle = std::size_t;
static constexpr NodeHandle InvalidNodeHandle = -1;

struct Transform
{
	glm::vec3 position = {0, 0, 0};
	glm::quat rotation = {1, 0, 0, 0};

	Transform() = default;
	
	explicit Transform(const glm::vec3& positionIn) :
		position{positionIn}
	{}

	Transform(const glm::vec3& positionIn, const glm::quat& rotationIn) :
		position{positionIn},
		rotation{rotationIn}
	{}
};

/*
	The scene graph stores nodes in a SoA format, using contiguous blocks of memory.
	Nodes are represented by ID (in this case, a string), local transform, world transform,
	and handle to a parent. The scene graph attempts to store its nodes in an order
	determined by the parent-child relationship. Parents are always stored before children
	so as to only require a single pass to update world transforms. In addition, children
	are stored as close as possible to their parents, to maximise the chance of parent
	transforms being in the cache when they are needed.
*/
struct SceneGraph
{
	NodeHandle PushBackNode(NodeID id, const Transform& localTransform, NodeHandle parenth = InvalidNodeHandle);
	NodeHandle InsertNode(NodeID id, const Transform& localTransform, NodeHandle parenth);
	void RemoveNode(NodeHandle nodeh);
	NodeHandle FindNode(const NodeID& id);
	void Reserve(std::size_t size);
	void Optimize();

	std::size_t GetNodeCount() const { return nodeCount; }
	NodeID GetNodeID(NodeHandle h) const { return ids[h]; }
	NodeHandle GetNodeParent(NodeHandle h) const { return parents[h]; }

	void SetLocalTransform(NodeHandle nodeh, const Transform& transform);
	void SetLocalPosition(NodeHandle nodeh, const glm::vec3& position);
	void SetLocalRotation(NodeHandle nodeh, const glm::quat& rotation);
	void SetParent(NodeHandle nodeh, NodeHandle parenth);

	glm::vec3 GetLocalPosition(NodeHandle nodeh);
	glm::vec3 GetWorldPosition(NodeHandle nodeh);
	glm::quat GetWorldRotation(NodeHandle nodeh);

	void UpdateWorldTransforms();

	// Update all nodes' local transforms using a custom update function
	template <class F>
	void UpdateLocalTransforms(const F& func)
	{
		for (Transform& localTransform : localTransforms)
		{
			func(localTransform);
		}
	}

	// Iterate all nodes' world transforms using a custom iteration function
	template <class F>
	void IterateWorldTransforms(const F& func) const
	{
		for (const Transform& worldTransform : worldTransforms)
		{
			func(worldTransform);
		}
	}
	
private:
	void MoveNode(NodeHandle nodeh, NodeHandle pos);

	// SoA layout, for cache locality
	std::size_t nodeCount = 0;
	std::vector<NodeID> ids;
	std::vector<Transform> localTransforms;
	std::vector<Transform> worldTransforms;
	std::vector<NodeHandle> parents;
};
