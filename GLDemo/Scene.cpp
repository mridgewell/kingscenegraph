
#include "Scene.h"

#include <GL/glut.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

static void DrawBox(const Transform& transform)
{
	glm::mat4 trans = glm::translate(glm::mat4(1.0f), transform.position);
	glm::mat4 rot   = glm::mat4_cast(transform.rotation);
	glm::mat4 mat   = trans * rot;

	glPushMatrix();
	glMultMatrixf(glm::value_ptr(mat));

	for(int i = 0; i < 6; i++)
	{
		glutSolidCube(2.0f);
	}
	glPopMatrix();
}

void DrawScene(const SceneGraph& sg)
{
	sg.IterateWorldTransforms(DrawBox);
}
