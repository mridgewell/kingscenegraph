
#include "Scene.h"

#include <chrono>
#include <GL/glut.h>
#include <glm/gtc/type_ptr.hpp>

static const int windowWidth = 1000;
static const int windowHeight = 600;

static SceneGraph sceneGraph;

static glm::vec4 lightDiffuse  = { 1.0f, 0.0f, 0.0f, 1.0f };
static glm::vec4 lightPosition = { 1.0f, 1.0f, 1.0f, 0.0f };

static glm::vec3 cameraPos = { 20, 15, 20 };
static glm::vec3 cameraTarget = { 0, 5, 0 };
static float cameraFOV = 60.0f;
static float cameraNear = 1.0f;
static float cameraFar = 1000.0f;

static void OnIdle()
{
	static const auto initialTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> clockTime = std::chrono::high_resolution_clock::now() - initialTime;
	float time = clockTime.count();

	sceneGraph.UpdateLocalTransforms(
		[&](Transform& localTransform) {
			time += 0.453f;
			const float bend    = sin(time) * 0.3f;
			const float twist   = sin(time * 0.2f) * 0.5f;
			localTransform.rotation = glm::quat(glm::vec3(0, twist, bend));
		}
	);

	sceneGraph.UpdateWorldTransforms();

	glutPostRedisplay();
}

static void OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	DrawScene(sceneGraph);
	glutSwapBuffers();
}

static void MakeTentacle(const glm::vec3& position)
{
	NodeHandle prev = sceneGraph.PushBackNode("root", Transform(position));
	for (int i = 0; i < 10; i++)
	{
		prev = sceneGraph.PushBackNode("child" + std::to_string(i), Transform(glm::vec3(0, 2.1f, 0)), prev);
	}
}

static void OnInit()
{
	MakeTentacle(glm::vec3());
	MakeTentacle(glm::vec3(16, 0, 0));
	MakeTentacle(glm::vec3(8, 0, 8));
	MakeTentacle(glm::vec3(-8, 0, 8));

	glEnable(GL_DEPTH_TEST);

	// Set up lighting
	glLightfv(GL_LIGHT0, GL_DIFFUSE, glm::value_ptr(lightDiffuse));
	glLightfv(GL_LIGHT0, GL_POSITION, glm::value_ptr(lightPosition));
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	
	// Setup the view/projection matrices
	glMatrixMode(GL_PROJECTION);
	gluPerspective(cameraFOV, static_cast<double>(windowWidth) / windowHeight, cameraNear, cameraFar);
	glMatrixMode(GL_MODELVIEW);
	const glm::vec3 cameraUp = { 0, 1, 0 };
	gluLookAt(cameraPos.x, cameraPos.y, cameraPos.z,
	          cameraTarget.x, cameraTarget.y, cameraTarget.z,
	          cameraUp.x, cameraUp.y, cameraUp.z);
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = 0;
	char* argv[] = {nullptr};
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("KingSceneGraph 3D Demo");
	glutDisplayFunc(OnRender);
	glutIdleFunc(OnIdle);
	OnInit();
	glutMainLoop();

	return 0;
}
